#!/bin/bash
# Run the dusk command.
php artisan dusk --debug --stop-on-failure
# Capture the code dusk exits with
exitcode=${PIPESTATUS[0]}

# Look for screenshots inside of the screenshots directory.
screenshots=(`find ./tests/Browser/screenshots/ -maxdepth 1 -name "*.png"`)

# Upload and show the link to the screenshots.
if [ ${#screenshots[@]} -gt 0 ]; then
    for filename in ./tests/Browser/screenshots/*.png; do
        file_str=${filename#*screenshots/}
        curl -s --upload-file "$filename" "https://transfer.sh/$file_str"
        echo ""
        exit 1
    done
else
    echo no screenshots found
fi

# Finally exit with the exit code the dusk command provided, when dusk fails we can still upload the screenshots now but fail the test.
exit $exitcode