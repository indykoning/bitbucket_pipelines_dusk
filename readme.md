# Bitbucket pipelines with dusk #
### Important! ###
The webdriver needs to have the folowing options enabled:
``--disable-gpu --headless --no-sandbox``
When using laravel the `DuskTestCase.php` would look like:
```php
protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--no-sandbox'
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }
```
Other than that you could just copy and paste these files and both PhpUnit and Dusktests will be executed.
## Note ##
this has been writen for use with laravel so this code might have to be changed a little bit to suit your needs.